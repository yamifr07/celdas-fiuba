package com.fiuba.resume;

import java.util.ArrayList;

public class ResumeParagraphTypeHeuristicProvider {

	public static ArrayList<ResumeParagraphTypeHeuristic> create(ArrayList<ResumeParagraph> paragraphs, ResumeParagraphType type) {
		ArrayList<ResumeParagraphTypeHeuristic> out = new ArrayList<ResumeParagraphTypeHeuristic>();
		if (type == ResumeParagraphType.PERSONAL_INFO) {
			out.add(new ResumeParagraphTypeHeuristicKeyword("PersonalInfo Keywords", paragraphs, "personalInfo.txt", false));
			out.add(new ResumeParagraphTypeHeuristicEmail("PersonalInfo Email", paragraphs));
			out.add(new ResumeParagraphTypeHeuristicFullName("PersonalInfo Known names", paragraphs, "names.txt"));
		} else if (type == ResumeParagraphType.EDUCATION) {
			out.add(new ResumeParagraphTypeHeuristicKeyword("Education Keywords", paragraphs, "education.txt", false));
			out.add(new ResumeParagraphTypeHeuristicKeyword("Education Institutions", paragraphs, "institutions.txt", false));
			out.add(new ResumeParagraphTypeHeuristicAverage("Education Average", paragraphs));
		} else if (type == ResumeParagraphType.WORK_HISTORY) {
			out.add(new ResumeParagraphTypeHeuristicKeyword("Work History Keywords", paragraphs, "workHistory.txt", false));
			out.add(new ResumeParagraphTypeHeuristicKeyword("Work History Technologies", paragraphs, "technologies.txt", false));
			out.add(new ResumeParagraphTypeHeuristicKeyword("Work History Companies", paragraphs, "companies.txt", false));
		} else if (type == ResumeParagraphType.HOBBIES) {
			out.add(new ResumeParagraphTypeHeuristicKeyword("Hobbies Keywords", paragraphs, "hobbies.txt", false));
			out.add(new ResumeParagraphTypeHeuristicKeyword("Hobbies Sports", paragraphs, "sports.txt", false));
		} else if (type == ResumeParagraphType.SKILLS) {
			out.add(new ResumeParagraphTypeHeuristicKeyword("Skills Keywords", paragraphs, "skills.txt", false));
			out.add(new ResumeParagraphTypeHeuristicKeyword("Skills tecnologies", paragraphs, "technologies.txt", false));
		}
		return out;
	}
}
