package com.fiuba.resume;

import java.util.ArrayList;

public class IntelligenceReference {

	private ArrayList<String> keywords;
	private ArrayList<String> previousKeywords;
	private ArrayList<String> delimiters;

    public void setPreviousKeywords(ArrayList<String> previousKeywords) {
        this.previousKeywords = previousKeywords;
    }

    public void setKeywords(ArrayList<String> keywords) {
        this.keywords = keywords;
    }

    public void setDelimiters(ArrayList<String> delimiters) {
        this.delimiters = delimiters;
    }

    public ArrayList<String> getKeywords() {
        return keywords;
    }
    
    public ArrayList<String> getDelimiters() {
        return delimiters;
    }

    public ArrayList<String> getPreviousKeywords() {
        return previousKeywords;
    }
    
}
