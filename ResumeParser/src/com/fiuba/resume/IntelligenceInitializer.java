package com.fiuba.resume;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class IntelligenceInitializer {

	private String keywordsFile;
	private String previousKeywordsFile;
	private String delimitersFile;

	public IntelligenceInitializer(String delimitersFile, String keywordsFile, String previousKeywordsFile) {
		this.keywordsFile = keywordsFile;
		this.previousKeywordsFile = previousKeywordsFile;
		this.delimitersFile = delimitersFile;
	}

	public IntelligenceReference create() {
		IntelligenceReference out = new IntelligenceReference();
		out.setDelimiters(this.getDelimiters());
		out.setKeywords(this.getKeywords());
		out.setPreviousKeywords(this.getPreviousKeywords());
		return out;
	}

	public ArrayList<String> getDelimiters() {
		ArrayList<String> delimiters = new ArrayList<String>();
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(new File(this.delimitersFile)));
			String str = in.readLine();
			while (str != null) {
				delimiters.add("" + str + "");
				str = in.readLine();
			}
		}

		// if the file is not found, the program displays an error message and
		// exits
		catch (IOException e) {
			System.out.println("The file " + this.delimitersFile + " was not found.");
			System.exit(0);
		}
		return delimiters;
	}

	private ArrayList<String> getKeywords() {
		ArrayList<String> keywords = new ArrayList<String>();
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(new File(this.keywordsFile)));
			String str = in.readLine();
			while (str != null) {
				keywords.add(str);
				str = in.readLine();
			}
		}
		// if the file is not found, the program displays an error message and
		// exits
		catch (IOException e) {
			System.out.println("The file " + this.keywordsFile
					+ " was not found.");
			System.exit(0);
		}
		return keywords;
	}

	private ArrayList<String> getPreviousKeywords() {
		ArrayList<String> previousKeywords = new ArrayList<String>();
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader(new File(this.previousKeywordsFile)));
			String str = in.readLine();
			while (str != null) {
				previousKeywords.add(str + ":");
				previousKeywords.add(str);
				str = in.readLine();
			}
		}
		// if the file is not found, the program displays an error message and
		// exits
		catch (IOException e) {
			System.out.println("The file " + this.previousKeywordsFile
					+ " was not found.");
			System.exit(0);
		}
		return previousKeywords;
	}

}
