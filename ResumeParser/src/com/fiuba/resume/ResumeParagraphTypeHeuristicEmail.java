package com.fiuba.resume;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class ResumeParagraphTypeHeuristicEmail extends ResumeParagraphTypeHeuristic {
	
	public ResumeParagraphTypeHeuristicEmail(String name, ArrayList<ResumeParagraph> paragraphs) {
		this.name = name;
		this.paragraphs = paragraphs;
	}

	public ArrayList<Float> getParagraphWeights() {
		ArrayList<Float> out = new ArrayList<Float>();
		int selectedIndex = -1;
		for (ResumeParagraph paragraph : this.paragraphs) {
			Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", Pattern.CASE_INSENSITIVE);
			Matcher matcher = pattern.matcher(paragraphs.toString());
			out.add((float) (matcher.find() ? 1 : 0));
		}
		return out;
	}
}
