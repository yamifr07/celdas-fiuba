package com.fiuba.resume;

import java.util.ArrayList;

import org.json.JSONObject;

public class WorkHistoryHeuristicGroup extends ParagraphTypeHeuristicGroup {

	public WorkHistoryHeuristicGroup(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		
		this.heuristicsForParagraphType = new ArrayList<ResumeParagraphTypeHeuristic>();
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Work History Keywords", paragraphs, "workHistory.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Work History Technologies", paragraphs, "technologies.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Work History Companies", paragraphs, "companies.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicPeriod("Work History Period", paragraphs, true, true));
		
		this.heuristicsForResumeAttribute = new ArrayList<ResumeAttributeHeuristic>();
	}
}
