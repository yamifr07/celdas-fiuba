package com.fiuba.resume;

import org.json.*;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class ResumeParserApp {

	private static ResumeParser parser;
	private static boolean parserUsedOneTime = false;
	
	public static void main(String[] args) {
    	
        String workingDir = System.getProperty("user.dir");
        System.out.println("Current working directory : " + workingDir);
        String outputFile = workingDir + "/output/objective.txt";
        ResumeParser myParser;
        boolean parserUsedOneTime = false;
        for (ArrayList<String> resumeData : getResumesToLearnData(workingDir + "/../data/cvsToLearn.txt")) {
        	System.out.println("Analysing " + resumeData.get(0));
        	HashMap<ResumeParagraphType, Integer> expectedParagraphIndex = new HashMap<ResumeParagraphType, Integer>();
	        
	        String resumeFile = workingDir + "/../data/" + resumeData.get(0);
	             
	        JSONObject expectedData = new JSONObject(resumeData.get(1));
	        myParser = getParser(resumeFile);
	        myParser.learn(expectedData);
	        
	        System.out.println("Expected data:" + expectedData.toString());
	        System.out.println("=====================");
	        
        }
        System.out.println("=====================");
        System.out.println("Learning is over, now let's analyze unknown cvs");
        getParser("").printLearningStatistics();
        System.out.println("=====================");
        for (String resumeFileName : getResumesToAnalyzeData(workingDir + "/../data/cvsToAnalyze.txt")) {
        	System.out.println("Analysing " + resumeFileName);
        	String resumeFile = workingDir + "/../data/" + resumeFileName;
	        myParser = getParser(resumeFile);
	        JSONObject data = myParser.analyze();
	        System.out.println("Extracted data: " + data.toString());
	        System.out.println("=====================");
	        
        }
    }
    
    private static ResumeParser getParser(String resumeFile) {
    	if (parserUsedOneTime) {
    		parser.setNewResumeFile(resumeFile);
    	} else {
    		parser = new ResumeParser(resumeFile);
    		parserUsedOneTime = true;
    	}
    	return parser;
    }
    
    private static ArrayList<String> getResumesToAnalyzeData(String resumesFile) {
    	ArrayList<String> out = new ArrayList<String>();
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader(resumesFile));
    	    String line;
    	    while ((line = br.readLine()) != null) {
    	    	if (line.trim().length() > 0) {
    	    		out.add(line);
    	    	}
    	    }
    	} catch (IOException e) {
			System.out.println("The file of resumes was not found.");
			System.exit(0);
		}
		return out;
    }
    
    private static ArrayList<ArrayList<String>> getResumesToLearnData(String resumesFile) {
    	ArrayList<ArrayList<String>> out = new ArrayList<ArrayList<String>>();
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader(resumesFile));
    	    String line;
    	    while ((line = br.readLine()) != null) {
    	    	String resumeFileName = line.substring(0, line.indexOf(","));
    	    	String expectedJsonData = line.substring(line.indexOf(",") + 1);
    	    	ArrayList<String> resumeData = new ArrayList<String>();
    	    	resumeData.add(resumeFileName);
    	    	resumeData.add(expectedJsonData);
    	    	out.add(resumeData);
    	    }
    	} catch (IOException e) {
			System.out.println("The file of resumes was not found.");
			System.exit(0);
		}
		return out;
    }
}
