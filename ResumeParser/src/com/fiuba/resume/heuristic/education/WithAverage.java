package com.fiuba.resume.heuristic.education;

import java.util.ArrayList;
import java.util.regex.*;

import com.fiuba.resume.IntelligenceReference;
import com.fiuba.resume.ResumeParagraph;
import com.fiuba.resume.heuristic.Heuristic;

public class WithAverage extends Heuristic {

	public WithAverage(IntelligenceReference intelligenceReference) {
		super(intelligenceReference);
	}

	@Override
	public String find(ArrayList<ResumeParagraph> resumeParagraphs) {
		String last = "";
		Pattern p = Pattern.compile("[0-9]{1,2}[\\\\,\\\\.][0-9]{1,2}");
		Matcher m = p.matcher("");
		for (ResumeParagraph resumeParagraph : resumeParagraphs) {
			m.reset(resumeParagraph.getText());
			if (m.find()) {
			   return last;
			} else if (resumeParagraph.getText().trim().length() > 0) {
				last = resumeParagraph.getText();
			}
		}
    	return null;
	}
	
	protected String getName() {
		return "Education:WithAverage";
	}
}
