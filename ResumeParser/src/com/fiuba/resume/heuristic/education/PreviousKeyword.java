package com.fiuba.resume.heuristic.education;

import java.util.ArrayList;

import com.fiuba.resume.IntelligenceReference;
import com.fiuba.resume.ResumeParagraph;
import com.fiuba.resume.heuristic.Heuristic;

public class PreviousKeyword extends Heuristic {

	public PreviousKeyword(IntelligenceReference intelligenceReference) {
		super(intelligenceReference);
	}

	@Override
	public String find(ArrayList<ResumeParagraph> resumeParagraphs) {
		boolean nextParagraphShouldBeOut = false;
		for (ResumeParagraph resumeParagraph : resumeParagraphs) {
			if (resumeParagraph.getText().trim().length() == 0) {
				continue;
			}
			String paragraphLower = resumeParagraph.getText().toLowerCase();
			if (nextParagraphShouldBeOut) {
				return resumeParagraph.getText();
			}
			for (String keyword : intelligenceReference.getPreviousKeywords()) {
				int pos = paragraphLower.indexOf(keyword);
				if (pos != -1) {
					String possibleOut = resumeParagraph.getText().substring(pos + keyword.length());
					if (possibleOut.trim().length() > 20) {
						return possibleOut;
					} else {
						nextParagraphShouldBeOut = true;
						break;
					}
				}
			}
		}
		return null;
	}
	
	protected String getName() {
		return "Education:PreviousKeyword";
	}

}
