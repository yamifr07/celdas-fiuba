package com.fiuba.resume.heuristic;

import java.util.ArrayList;

import com.fiuba.resume.HeuristicResult;
import com.fiuba.resume.IntelligenceReference;
import com.fiuba.resume.ResumeParagraph;

public abstract class Heuristic {

	private String lastFound;
	private String lastExpected;

	private HeuristicResult result = new HeuristicResult();
	protected IntelligenceReference intelligenceReference;

	public Heuristic(IntelligenceReference intelligenceReference) {
		this.intelligenceReference = intelligenceReference;
	}

	public void analyze(ArrayList<ResumeParagraph> resumeParagraphs,
			String expectedResult) {
		this.lastExpected = expectedResult;
		this.lastFound = this.find(resumeParagraphs);
		this.result.log(this.lastFound, this.lastExpected);
	}

	public String toString() {
		String out = "   " + this.getName() + ": " + this.result.toString();
		if (true) {
			out += " (Found \"" + this.lastFound + "\" while looking \""
					+ this.lastExpected + "\")";
		}
		return out;
	}

	protected abstract String find(ArrayList<ResumeParagraph> resumeParagraphs);

	protected abstract String getName();

}
