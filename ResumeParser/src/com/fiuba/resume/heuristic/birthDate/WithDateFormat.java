package com.fiuba.resume.heuristic.birthDate;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.fiuba.resume.IntelligenceReference;
import com.fiuba.resume.ResumeParagraph;
import com.fiuba.resume.heuristic.Heuristic;

public class WithDateFormat extends Heuristic {

	public WithDateFormat(IntelligenceReference intelligenceReference) {
		super(intelligenceReference);
	}

	@Override
	public String find(ArrayList<ResumeParagraph> resumeParagraphs) {
		ArrayList<String> out;
		for (ResumeParagraph resumeParagraph : resumeParagraphs) {
			if (resumeParagraph.getText().trim().isEmpty()) {
				continue;
			}

			String[] words = resumeParagraph.getText().split(" ");
			for (String word : words) {
				if (word.matches("[0-9]{1,2}[\\\\-\\\\/][0-9]{1,2}[\\\\-\\\\/][0-9]{2,4}")) {
					return word;
				}
			}
		}
		return null;
	}
	
	protected String getName() {
		return "BirthDate:WithDateFormat";
	}
}
