package com.fiuba.resume.heuristic.birthDate;

import java.util.ArrayList;

import com.fiuba.resume.IntelligenceReference;
import com.fiuba.resume.ResumeParagraph;
import com.fiuba.resume.heuristic.Heuristic;

public class PreviousKeyword extends Heuristic {

	public PreviousKeyword(IntelligenceReference intelligenceReference) {
		super(intelligenceReference);
	}

	@Override
	public String find(ArrayList<ResumeParagraph> resumeParagraphs) {
		for (ResumeParagraph resumeParagraph : resumeParagraphs) {
			String paragraphLower = resumeParagraph.getText().toLowerCase();
			for (String keyword : intelligenceReference.getPreviousKeywords()) {
				int pos = paragraphLower.indexOf(keyword);
				if (pos != -1) {
					return resumeParagraph.getText().substring(pos + keyword.length());
				}
			}
		}
		return null;
	}

	protected String getName() {
		return "BirthDate:PreviousKeyword";
	}
}

