package com.fiuba.resume;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.json.JSONObject;

public class EducationEntryHeuristic extends ResumeAttributeHeuristic {

	public EducationEntryHeuristic(String name) {
		super(name);
	}

	public JSONObject extractInfo(ArrayList<ResumeParagraph> selectedParagraphs) {
		JSONObject out = new JSONObject();
		ArrayList<String> institutions = new ArrayList<String>();
		ArrayList<String> institutionsKeywords = this.getKeywords("institutionsEntries.txt");
		ArrayList<String> commonWords = this.getKeywords("commonWords.txt");
		for (ResumeParagraph p : selectedParagraphs) {
			String[] words = p.getTextWithoutSpecialChars().split("\\s");
			for (int i = 0; i < words.length; i++) {
				String word = words[i];
				String institution = "";
				if (institutionsKeywords.contains(word.toLowerCase())) {
					int j = 0;
					while (j + i < words.length && 
						(
							this.getWordCaseType(word) ==  this.getWordCaseType(words[i + j]) ||
							this.isUpperCase(words[i + j]) ||
							commonWords.contains(words[i + j].toLowerCase())
						)
					) {
						institution += " " + words[i + j];
						j++;
					}
					if (j > 1) {
						institutions.add(institution.substring(1));
					}
					i += j + 1;
				}
			}
			
		}
		ArrayList<HashMap<String, String>> educationEntries = new ArrayList<HashMap<String, String>>(); 
		for (String i : institutions) {
			HashMap<String, String> map = new HashMap<String, String>();
			map.put("institution", i);
			educationEntries.add(map);
		}
		out.put("education", educationEntries);
		return out;
	}

}
