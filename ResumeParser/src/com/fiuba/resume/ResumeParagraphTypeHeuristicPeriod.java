package com.fiuba.resume;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class ResumeParagraphTypeHeuristicPeriod extends ResumeParagraphTypeHeuristic {
	
	private boolean yearsAndMonths;
	
	public ResumeParagraphTypeHeuristicPeriod(String name, ArrayList<ResumeParagraph> paragraphs, boolean yearsAndMonths, boolean priorityForFirstParagraphs) {
		this.name = name;
		this.paragraphs = paragraphs;
		this.yearsAndMonths = yearsAndMonths;
		this.priorityForFirstParagraphs = priorityForFirstParagraphs;
	}
	
	private String getMonthsForRegex() {
		return "Ene|Enero|Feb|Febrero|Mar|Marzo|Abr|Abril|May|Mayo|Jun|Junio|Jul|Julio|Ago|Agosto|Sep|Septiembre|Set|Setiempre|Oct|Octubre|Nov|Noviembre|Dic|Diciembre";
	}
	
	public ArrayList<Float> getParagraphWeights() {
		ArrayList<Float> out = new ArrayList<Float>();
		ArrayList<Pattern> patterns = new ArrayList<Pattern>();
		if (this.yearsAndMonths == true) {
			patterns.add(Pattern.compile("[0-9]{1,2}[\\\\.\\\\-\\\\b]*[0-9]{2,4}"));
			patterns.add(Pattern.compile("[" + this.getMonthsForRegex() + "][\\-\\.\\\\b]*[0-9]{2,4}"));
		} else {
			patterns.add(Pattern.compile("[0-9]{4}[\\-\\\\b]*[0-9]{4}"));
			patterns.add(Pattern.compile("(19|20)[0-9]{2}"));
		}		
		for (ResumeParagraph paragraph : this.paragraphs) {
			float found = 0;
			for (Pattern p : patterns) {
				Matcher matcher = p.matcher(paragraph.getText());
				if (matcher.find()) {
					found += this.getWithPriorityForFirstParagraphsIfNecessary(1, paragraph.getIndex());
				}
			}
			out.add(found);
		}
		return out;
	}
}
