package com.fiuba.resume;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONObject;

public class PersonalInfoNameHeuristic extends ResumeAttributeHeuristic {

	ArrayList<String> knownNames;
	
	public PersonalInfoNameHeuristic(String name, String knownNamesFileName) {
		super(name);
		this.knownNames = this.getKeywords(knownNamesFileName);
	}
	
	public JSONObject extractInfo(ArrayList<ResumeParagraph> selectedParagraphs) {
		JSONObject out = new JSONObject();
		out.put("fullName", "notFound");
		for (ResumeParagraph p : selectedParagraphs) {
    		String[] words = p.getTextWithoutSpecialCharsExceptDotAndCommas().split(" ");
    		int length = words.length;
    		int start = -1;
    		int end = -1;
        	for (int i = 0; i < length - 1; i++) {
        		String word = words[i];
        		if (knownNames.contains(word.toLowerCase())) {
        			start = i;
        			end = i + 1;
        			if (words[i + 1].matches("[A-ZÁÉÍÓÚ]\\.")) {
        				end++;
        			}
        			if (words.length > end && knownNames.contains(words[end].toLowerCase())) {
        				end++;
        			}
        			while (words.length > end && words[end].matches("[A-ZÁÉÍÓÚ]+") && this.getWordCaseType(words[i]) == this.getWordCaseType(words[end])) {
        				end++;
        			}
        			while (words.length > end && words[end].matches("[A-ZÁÉÍÓÚ][a-záéíóú]+") && this.getWordCaseType(words[i]) == this.getWordCaseType(words[end])) {
        				end++;
        			}
        			if (start > 0 && words[start - 1].matches("[A-Za-záéíóúÁÉÍÓÚ]+\\,")) {
        				start--;
        			}
        			
        			if (end > start + 1) {
            			String fullName = "";
            			for (int j = start; j < end; j++) {
            				fullName += " " + words[j];
            			}
            			out.put("fullName", fullName.substring(1));
            			return out;
            		}
        		}
        		
        	}
        }
		return out;
	}
	

	
	protected ArrayList<String> getKeywords(String knownNamesFileName) {
		String workingDir = System.getProperty("user.dir");
		ArrayList<String> out = new ArrayList<String>();
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader(workingDir + "/intelligence/paragraphType/" + knownNamesFileName));
    		String line;
    	    while ((line = br.readLine()) != null) {
    	    	out.add(line.toLowerCase());
    	    }
    	    br.close();
    	} catch (IOException e) {
			System.out.println("The file of keywords was not found.");
			System.exit(0);
		}
		return out;
	}

}
