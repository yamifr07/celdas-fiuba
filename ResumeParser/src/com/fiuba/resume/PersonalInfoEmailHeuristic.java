package com.fiuba.resume;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class PersonalInfoEmailHeuristic extends ResumeAttributeHeuristic {
	
	public PersonalInfoEmailHeuristic(String name) {
		super(name);
	}

	public JSONObject extractInfo(ArrayList<ResumeParagraph> selectedParagraphs) {
		JSONObject out = new JSONObject();
		out.put("email", "notFound");
		Pattern pattern = Pattern.compile("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", Pattern.CASE_INSENSITIVE);
		for (ResumeParagraph p : selectedParagraphs) {
			Matcher matcher = pattern.matcher(p.getText());
			if (matcher.find()) {
				out.put("email", matcher.group());
				break;
			}
		}
		return out;
	}

}
