package com.fiuba.resume;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import com.uwyn.jhighlight.fastutil.Hash;

public abstract class ParagraphTypeHeuristicGroup {

	protected ArrayList<ResumeParagraphTypeHeuristic> heuristicsForParagraphType;
	protected ArrayList<ResumeAttributeHeuristic> heuristicsForResumeAttribute;
	protected ArrayList<ResumeParagraph> paragraphs;
	protected int tries;
	
	public void setParagraphs(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		for (ResumeParagraphTypeHeuristic h : this.heuristicsForParagraphType) {
			h.setParagraphs(paragraphs);
		}
	}
	
	public void printLearningStatistics() {
		for (ResumeAttributeHeuristic h : this.heuristicsForResumeAttribute) {
			h.print();
		}
	}
	
	public JSONObject analyze() {
		ArrayList<ResumeParagraph> selectedParagraphs = this.getSelectedParagraphs();
		JSONObject totalResult = new JSONObject();
		for (ResumeAttributeHeuristic h : this.heuristicsForResumeAttribute) {
			totalResult = this.mergeResults(totalResult, h.extractInfo(selectedParagraphs));
		}
		return totalResult;
	}
	
	public void learn(JSONObject expectedData) {
		this.tries++;
		ArrayList<ResumeParagraph> selectedParagraphs = this.getSelectedParagraphs();
		JSONObject resultToLog = new JSONObject(); 
		for (ResumeAttributeHeuristic h : this.heuristicsForResumeAttribute) {
			JSONObject result = h.extractInfo(selectedParagraphs);
			resultToLog = this.mergeResults(resultToLog, result);
			int success = this.compare(result, expectedData);
			h.addSuccess(success);
		}
		String selectedParagprahIndexes = "";
		for (ResumeParagraph p : selectedParagraphs) {
			selectedParagprahIndexes += " - " + p.getIndex();
		}
		System.out.println("Selected paragraphs by " + this.getClass().toString()  + ": " + (selectedParagprahIndexes.length() > 3 ? selectedParagprahIndexes.substring(3) : selectedParagprahIndexes));
		System.out.println("Extracted data: " + resultToLog.toString());
	}
	
	private int compareKey(JSONObject data, JSONObject expectedData, String key) {
		if (data.has(key) && expectedData.has(key)) {
			if (data.getString(key).equals("notFound")) {
				return 0;
			}
			return (int) (100 * this.getSimilarity(data.getString(key), expectedData.getString(key)));
		} else {
			return 0;
		}
	}
	
	private int compare(JSONObject data, JSONObject expectedData) {
		int total = 0;
		total += this.compareKey(data, expectedData, "email");
		total += this.compareKey(data, expectedData, "birthDate");
		total += this.compareKey(data, expectedData, "fullName");
		return total;
	}
	
	protected int getSelectedIndexLimit() {
		return 4;
	}
	
	private JSONObject mergeResults(JSONObject o1, JSONObject o2) {
		JSONObject mergedObj = new JSONObject();

		Iterator i1 = o1.keys();
		Iterator i2 = o2.keys();
		String tmp_key;
		while(i1.hasNext()) {
		    tmp_key = (String) i1.next();
		    mergedObj.put(tmp_key, o1.get(tmp_key));
		}
		while(i2.hasNext()) {
		    tmp_key = (String) i2.next();
		    mergedObj.put(tmp_key, o2.get(tmp_key));
		}
		return mergedObj;
	}
	
	private ArrayList<ResumeParagraph> getSelectedParagraphs() {
		HashMap<Integer, Float> selectedIndexesCount = new HashMap<Integer, Float>();
		for (ResumeParagraphTypeHeuristic h : this.heuristicsForParagraphType) {
			ArrayList<Float> paragraphWeights = h.getParagraphWeights();
			int paragraphIndex = -1;
			for (Float weight : paragraphWeights) {
				paragraphIndex++;
				if (paragraphWeights.get(paragraphIndex) <= 0) {
					continue;
				}
				if (!selectedIndexesCount.containsKey(paragraphIndex)) {
					selectedIndexesCount.put(paragraphIndex, (float) 0);
				}
				selectedIndexesCount.put(paragraphIndex, selectedIndexesCount.get(paragraphIndex) + paragraphWeights.get(paragraphIndex));
			}
		}

		ArrayList<Integer> sortedIndexes = sortByValues(selectedIndexesCount);
		ArrayList<ResumeParagraph> selectedParagraphs = new ArrayList<ResumeParagraph>();
		for (int i = 0; i < sortedIndexes.size() && i < this.getSelectedIndexLimit(); i++) {
			selectedParagraphs.add(this.paragraphs.get(sortedIndexes.get(i)));
		}
		return selectedParagraphs;
	}
	
	private static ArrayList<Integer> sortByValues(HashMap map) { 
			LinkedList list = new LinkedList(map.entrySet());
	       // Defined Custom Comparator here
	       Collections.sort((java.util.List<Integer>) list, new Comparator() {
	            public int compare(Object o1, Object o2) {
	               return -1 * ((Comparable) ((Map.Entry) (o1)).getValue())
	                  .compareTo(((Map.Entry) (o2)).getValue());
	            }
	       });

	       // Here I am copying the sorted list in HashMap
	       // using LinkedHashMap to preserve the insertion order
	       HashMap sortedHashMap = new LinkedHashMap();
	       for (Iterator it = ((java.util.List<ResumeParagraphTypeHeuristic>) list).iterator(); it.hasNext();) {
	              Map.Entry entry = (Map.Entry) it.next();
	              sortedHashMap.put(entry.getKey(), entry.getValue());
	       } 
	       ArrayList<Integer> out = new ArrayList<Integer>();
	       for (Object o : sortedHashMap.keySet().toArray()) {
	    	   out.add((Integer) o);
	       }
	       return out;
	  }
	
	private ArrayList<Integer> sortHashMapByValues(HashMap<Integer, Float> passedMap) {
		ArrayList<Integer> mapKeys = new ArrayList<Integer>(passedMap.keySet());
		ArrayList<Float> mapValues = new ArrayList<Float>(passedMap.values());
	   	Collections.sort(mapValues);
	   	Collections.sort(mapKeys);

		ArrayList<Integer> sortedIndexes = new ArrayList<Integer>();

		   Iterator<Float> valueIt = mapValues.iterator();
		   while (valueIt.hasNext()) {
			   Float val = valueIt.next();
		       Iterator keyIt = mapKeys.iterator();
	           Object key = keyIt.next();
	           Float comp1 = passedMap.get(key);
	           
		       while (keyIt.hasNext()) {
		           Float comp2 = val;
		           if (comp1.equals(comp2)){
		               passedMap.remove(key);
		               mapKeys.remove(key);
		               sortedIndexes.add((Integer) key);
		               break;
		           }

		       }

		   }
		   return sortedIndexes;
		}
	
	private double getSimilarity(String actual, String expected) {
		return ((double) Math.floor(similarity(actual, expected) * 5)) / 5;
	}
	
    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) { // longer should always have greater length
            longer = s2; shorter = s1;
        }
        int longerLength = longer.length();
        if (longerLength == 0) { return 1.0; /* both strings are zero length */ }
        /* // If you have StringUtils, you can use it to calculate the edit distance:
        return (longerLength - StringUtils.getLevenshteinDistance(longer, shorter)) /
                                                             (double) longerLength; */
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
 
    }
 
    // Example implementation of the Levenshtein Edit Distance
    // See http://r...content-available-to-author-only...e.org/wiki/Levenshtein_distance#Java
    public static int editDistance(String s1, String s2) {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
 
        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++) {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0)
                    costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue),
                                    costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0)
                costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }

	private boolean areSimilar(String actual, String expected) {
		return StringUtils.getLevenshteinDistance(actual, expected) < 3;
	}
	
}
