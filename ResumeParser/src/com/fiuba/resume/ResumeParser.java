package com.fiuba.resume;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.json.JSONObject;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ResumeParser {
	
	private HashMap<ResumeParagraphType, ArrayList<ResumeParagraphTypeHeuristic>> heuristicsByType;
	private ArrayList<ParagraphTypeHeuristicGroup> heuristicsGroups;
	
	private String resumeFile;
	
	public ResumeParser(String resumeFile) {
		this.resumeFile = resumeFile;
		this.heuristicsGroups = new ArrayList<ParagraphTypeHeuristicGroup>();
		ArrayList<ResumeParagraph> paragraphs = this.getParagraphs();
		this.heuristicsGroups.add(new PersonalInfoHeuristicGroup(paragraphs));
		this.heuristicsGroups.add(new HobbiesHeuristicGroup(paragraphs));
		this.heuristicsGroups.add(new SkillsHeuristicGroup(paragraphs));
		this.heuristicsGroups.add(new WorkHistoryHeuristicGroup(paragraphs));
		this.heuristicsGroups.add(new EducationHeuristicGroup(paragraphs));
		
//		this.heuristicsByType = new HashMap<ResumeParagraphType, ArrayList<ResumeParagraphTypeHeuristic>>();
//		for (ResumeParagraphType type : ResumeParagraphType.values()) {
//			this.miheuristicsByType.put(type, ResumeParagraphTypeHeuristicProvider.create(paragraphs, type));
//		}
	}
	
	public void setNewResumeFile(String resumeFile) {
		if (resumeFile == "") {
			return;
		}
		this.resumeFile = resumeFile;
		ArrayList<ResumeParagraph> paragraphs = this.getParagraphs();
		for (ParagraphTypeHeuristicGroup g : this.heuristicsGroups) {
			g.setParagraphs(paragraphs);
		}
	}
	
	public void learn(JSONObject expectedData) {
		for (ParagraphTypeHeuristicGroup g : this.heuristicsGroups) {
			g.learn(expectedData);
		}
	}
	
	public void printLearningStatistics() {
		for (ParagraphTypeHeuristicGroup g : this.heuristicsGroups) {
			g.printLearningStatistics();
		}
	}
	
	private ArrayList<String> getPossibleKeys() {
		ArrayList<String> out = new ArrayList<String>();
		out.add("fullName");
		out.add("email");
		out.add("birthDate");
		out.add("skills");
		out.add("education");
		return out;
	}
	
	public JSONObject analyze() {
		JSONObject fullData = new JSONObject();
		for (ParagraphTypeHeuristicGroup g : this.heuristicsGroups) {
			JSONObject data = g.analyze();
			for (String key : this.getPossibleKeys()) {
				if (data.has(key)) {
					fullData.put(key, data.get(key));
//					fullData.put(key + "P", data.get(key + "P"));
				}
			}
		}
		return fullData;
	}
	
	public void printLearned() {
		for (ResumeParagraphType type : ResumeParagraphType.values()) {
			for (ResumeParagraphTypeHeuristic heuristic : heuristicsByType.get(type)) {
				heuristic.printLearned();
			}
		}
	}
	
//	public void getResult() {
//		
//		for (ResumeParagraphType type : ResumeParagraphType.values()) {
//			for (ResumeParagraphTypeHeuristic heuristic : heuristicsByType.get(type)) {
//				heuristic.getResult();
//			}
//		}
//		
//		for (ResumeParagraph paragraph : paragraphs) {
//			HashMap<ResumeParagraphType, Integer> weights = new HashMap<ResumeParagraphType, Integer>(); 
//				
//				ArrayList<String> keywords;
//				
//			Integer total = 0;
//			for (Integer weight : weights.values()) {
//				total += weight;
//			}
//			if (total == 0) {
//				// This paragraph has unrecognized content, what to do?
//			} else {
//				for (ResumeParagraphType type : ResumeParagraphType.values()) {
//					if (weights.containsKey(type)) {
//						paragraph.setWeightForType(type, ((float) weights.get(type)) / total);
//					}
//				}
//			}
//		}
//		return paragraphs;
//	}
	
	private ArrayList<ResumeParagraph> getParagraphs() {
		ArrayList<ResumeParagraph> out = new ArrayList<ResumeParagraph>();
		InputStream is = null;
        try {
            is = new FileInputStream(new File(this.resumeFile));
            Parser parser = new AutoDetectParser();
            ContentHandler handler = new BodyContentHandler();
            Metadata metadata = new Metadata();
            parser.parse(is, handler, metadata, new ParseContext());
            String enter = "\\n\\n";
            
            String[] possibleParagraphs = handler.toString().split(enter);
            String concatToNextParagraphText = "";
            boolean lastParagraphWasATitle = false;
            int indexCounter = 0;
            for (String possibleParagraph : possibleParagraphs) {
            	boolean addedToLastParagraph = false;
            	if (possibleParagraph.trim().length() == 0) {
            		continue;
            	}
            	String firstChar = possibleParagraph.substring(0, 1);
            	if (lastParagraphWasATitle ||  firstChar.matches("[a-z\\-\\t]")) {
            		//This possible paragraph is the continuation of the last paragraph
            		out.get(out.size() -1).addToText(" " + possibleParagraph);
            		addedToLastParagraph = true;
            	} else {
            		addedToLastParagraph = false;
            	}
            	if (
        			possibleParagraph.endsWith(":") 
        			|| possibleParagraph.toUpperCase().equals(possibleParagraph)
        			|| (possibleParagraph.split(" ").length <= 2 && possibleParagraph.matches("[A-Za-z\\s ]*"))
            	) {
            		//This possible paragraph is the title of the next a title
            		lastParagraphWasATitle = true;
            	} else {
            		lastParagraphWasATitle = false;
            	}
            	
            	if (addedToLastParagraph != true) {
	            	ResumeParagraph rp = new ResumeParagraph(possibleParagraph);
	            	rp.setIndex(indexCounter++);
	            	out.add(rp);
            	}
            }
            for (ResumeParagraph p : out) {
            	System.out.println("Paragraph " + p.getIndex() + ": " + p.getText());
            }
            return out;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
	}
}
