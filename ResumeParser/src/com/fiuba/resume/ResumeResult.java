package com.fiuba.resume;

abstract public class ResumeResult {

	abstract public ResumeParagraphType getType();
	abstract public String getString();
	
}
