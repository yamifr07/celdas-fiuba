package com.fiuba.resume;

import java.util.ArrayList;

public class ResumeParagraphTypeHeuristicFullName extends ResumeParagraphTypeHeuristicKeyword {
	
	public ResumeParagraphTypeHeuristicFullName(String name, ArrayList<ResumeParagraph> paragraphs, String namesFileName) {
		super(name, paragraphs, namesFileName, true);
	}
}
