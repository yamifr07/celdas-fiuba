package com.fiuba.resume;

import java.util.ArrayList;

import org.json.JSONObject;

public class HobbiesHeuristicGroup extends ParagraphTypeHeuristicGroup {

	public HobbiesHeuristicGroup(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		
		this.heuristicsForParagraphType = new ArrayList<ResumeParagraphTypeHeuristic>();
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Hobbies Keywords", paragraphs, "hobbies.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Hobbies Sports", paragraphs, "sports.txt", false));
		
		this.heuristicsForResumeAttribute = new ArrayList<ResumeAttributeHeuristic>();
	}
}
