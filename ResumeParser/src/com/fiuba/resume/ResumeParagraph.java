package com.fiuba.resume;

import java.util.HashMap;

public class ResumeParagraph {

	private int index;
	private String text;
	private HashMap<ResumeParagraphType, Float> resumeParagraphTypes;
	
	public ResumeParagraph(String text) {
		this.text = text;
		this.purgeText();
	}
	
	private void purgeText() {
		this.text = this.text.replace("\t", "");
	}
	
	public void addToText(String text) {
		this.text += text;
	}
	
	public String getTextWithoutSpecialChars() {
		return this.text.replaceAll("[\\n]", " ").replace(":", "").replace("-", "").replace(".", "").replace(";", "").replace(",", "").replace("+", "");
	}
	
	public String getTextWithoutSpecialCharsExceptDot() {
		return this.text.replaceAll("[\\n]", " ").replace(":", "").replace("-", "").replace(";", "").replace(",", "").replace("+", "");
	}
	
	public String getTextWithoutSpecialCharsExceptDotAndCommas() {
		return this.text.replaceAll("[\\n]", " ").replace(":", "").replace("-", "").replace(";", "").replace("+", "");
	}
	
	public String getText() {
		return this.text;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public int getIndex() {
		return this.index;
	}
	
	public Float getWeightForType(ResumeParagraphType type) {
		if (!resumeParagraphTypes.containsKey(type)) {
			return new Float(0);
		}
		return resumeParagraphTypes.get(type);
	}
	
	public void setWeightForType(ResumeParagraphType type, Float weight) {
		try {
			if (weight.compareTo(new Float(1)) == 1 || weight.compareTo(new Float(0)) == -1) {
				throw new Exception("nvalidWeight");
			}
			this.resumeParagraphTypes.put(type, weight);
		} catch (Exception e) {
            e.printStackTrace();
		}
	}
}
