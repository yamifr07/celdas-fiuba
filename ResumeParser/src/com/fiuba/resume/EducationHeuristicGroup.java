package com.fiuba.resume;

import java.util.ArrayList;

public class EducationHeuristicGroup extends ParagraphTypeHeuristicGroup {

	public EducationHeuristicGroup(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		this.heuristicsForParagraphType = new ArrayList<ResumeParagraphTypeHeuristic>();
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Education Keywords", paragraphs, "education.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Education Institutions", paragraphs, "institutions.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicAverage("Education Average", paragraphs));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicPeriod("Education Period", paragraphs, false, true));
		
		this.heuristicsForResumeAttribute = new ArrayList<ResumeAttributeHeuristic>();
		this.heuristicsForResumeAttribute.add(new EducationEntryHeuristic("Education Entry"));
	}

}
