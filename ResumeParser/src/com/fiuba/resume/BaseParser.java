package com.fiuba.resume;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import com.fiuba.resume.heuristic.Heuristic;

public abstract class BaseParser {

    private ParseHelper parseHelper;
    private ArrayList<Heuristic> heuristics;
    private String expectedResult;
    private String outputFile;
    protected IntelligenceReference intelligenceReference;
    protected ArrayList<ResumeParagraph> resumeParagraphs; 
    
    abstract protected ArrayList<Heuristic> createHeuristics();
    abstract protected String getName();
    abstract protected void analyzeParagraph(ResumeParagraph pr);

    public BaseParser(String resumeFile, String outputFile, String expectedResult, final IntelligenceReference intelligenceReference) {
        this.outputFile = outputFile;
        this.expectedResult = expectedResult;
        this.intelligenceReference = intelligenceReference;
        this.heuristics = this.createHeuristics();
        parseHelper = new ParseHelper(resumeFile, outputFile);
        this.resumeParagraphs = parseHelper.getResumeParagraphs();
        this.analyzeParagraphsWeights();
    }
    
    private void analyzeParagraphsWeights() {
    	for (ResumeParagraph pr : this.resumeParagraphs) {
    		this.analyzeParagraph(pr);
    	}
    } 
    
    public ArrayList<String> split(String str) {
    	ArrayList<String> lines = new ArrayList<String>();
        if (str == null || str.length() == 0) {
            return lines;
        }
        String delimiters = "";
    	int i = 0;
    	int length = intelligenceReference.getDelimiters().size();
    	for (String delim : intelligenceReference.getDelimiters()) {
    		delimiters += delim;
            if (i != length - 1) {
            	delimiters += "|";
            }
            i++;
        }
    	return new ArrayList<String>(Arrays.asList(str.split(delimiters)));
    }
    
    public void analyze() {
    	for (Heuristic h : this.heuristics) {
    		h.analyze(this.resumeParagraphs, this.expectedResult);
    	}
    }

    private void display() {
        System.out.println(this.getName());
        for (Heuristic h : this.heuristics) {
            System.out.println(h.toString());
        }

    }

    public void output() {
        if (outputFile == null || outputFile.length() == 0) {
            display();
        } else {
            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(new File(outputFile)));
            } catch (IOException e) {
                System.out.println("An error occurred while writing to file: " + outputFile);
                System.out.println("The results have instead been displayed below.");
                display();
            }
        }
    }
    
   public void getInfo() {
        long start = System.currentTimeMillis();
        this.analyze();
        this.output();
        this.display();
        long end = System.currentTimeMillis();
        System.out.println("Time taken: " + (end - start) + "ms");
    }
}
