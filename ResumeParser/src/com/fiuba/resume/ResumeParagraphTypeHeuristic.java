package com.fiuba.resume;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

public abstract class ResumeParagraphTypeHeuristic {

	protected int success;
	protected String name;
	protected ArrayList<ResumeParagraph> paragraphs;
	protected boolean priorityForFirstParagraphs = false;
	
	public void setParagraphs(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
	}
	
	public void printLearned() {
		System.out.println(this.name + ": " + this.success);
	}
	
//	abstract void learn(JSONObject expectedData);
	abstract ArrayList<Float> getParagraphWeights();
	
	public void addSuccess(int success) {
		this.success += success;
	}
	
	protected float getWithPriorityForFirstParagraphsIfNecessary(float weight, int paragraphIndex) {
		float sub;
		if (paragraphIndex < 0.33 * this.paragraphs.size()) {
			sub = 0;
		} else if (paragraphIndex < 0.66 * this.paragraphs.size()) {
			sub = weight / 4;
		} else {
			sub = weight / 3;
		}
		return weight - sub;
	}
}
