package com.fiuba.resume;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class ResumeParagraphTypeHeuristicAverage extends ResumeParagraphTypeHeuristic {
	
	public ResumeParagraphTypeHeuristicAverage(String name, ArrayList<ResumeParagraph> paragraphs) {
		this.name = name;
		this.paragraphs = paragraphs;
	}

	public ArrayList<Float> getParagraphWeights() {
		ArrayList<Float> out = new ArrayList<Float>();
		Pattern p = Pattern.compile("[0-9]{1,2}[\\\\,\\\\.][0-9]{1,2}");
		Matcher m = p.matcher("");
		for (ResumeParagraph paragraph : this.paragraphs) {
			String text = paragraph.getText();
			m.reset(text);
			String result = m.replaceAll("");
			float found = (text.length() - result.length()) / 4;
			out.add(found);
		}
		return out;
	}

	JSONObject extractInfo() {
		// TODO Auto-generated method stub
		return null;
	}
}
