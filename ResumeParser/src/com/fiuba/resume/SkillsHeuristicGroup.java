package com.fiuba.resume;

import java.util.ArrayList;

import org.json.JSONObject;

public class SkillsHeuristicGroup extends ParagraphTypeHeuristicGroup {

	public SkillsHeuristicGroup(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		
		this.heuristicsForParagraphType = new ArrayList<ResumeParagraphTypeHeuristic>();
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Skills Keywords", paragraphs, "skills.txt", false));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("Skills tecnologies", paragraphs, "technologies.txt", false));
		
		this.heuristicsForResumeAttribute = new ArrayList<ResumeAttributeHeuristic>();
	}
}
