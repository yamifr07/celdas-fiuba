package com.fiuba.resume;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ResumeParagraphTypeHeuristicKeyword extends ResumeParagraphTypeHeuristic {

	protected ArrayList<String> keywords;
	
	
	public ResumeParagraphTypeHeuristicKeyword(String name, ArrayList<ResumeParagraph> paragraphs, String keywordsFileName, boolean priorityForFirstParagraphs) {
		this.name = name;
		this.paragraphs = paragraphs;
		this.keywords = this.getKeywords(keywordsFileName);
		this.priorityForFirstParagraphs = priorityForFirstParagraphs;
	}
	
	public ArrayList<Float> getParagraphWeights() {
		ArrayList<Float> out = new ArrayList<Float>();
		for (ResumeParagraph paragraph : this.paragraphs) {
			ArrayList<String> wordsInParagraph = new ArrayList<String>(java.util.Arrays.asList(paragraph.getTextWithoutSpecialChars().toLowerCase().split(" ")));
			ArrayList<String> cloneedKeywords = (ArrayList<String>) this.keywords.clone();
			cloneedKeywords.retainAll(wordsInParagraph);
			out.add(this.getWithPriorityForFirstParagraphsIfNecessary(cloneedKeywords.size(), paragraph.getIndex()));
		}
		return out;
	}
	
	private ArrayList<String> getKeywords(String keywordsFile) {
		String workingDir = System.getProperty("user.dir");
		ArrayList<String> out = new ArrayList<String>();
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader(workingDir + "/intelligence/paragraphType/" + keywordsFile));
    		String line;
    	    while ((line = br.readLine()) != null) {
    	    	if (line.trim().length() > 0) {
    	    		out.add(line.toLowerCase());
    	    	}
    	    }
    	    br.close();
    	} catch (IOException e) {
			System.out.println("The file of keywords was not found.");
			System.exit(0);
		}
		return out;
	}
}
