package com.fiuba.resume;

import java.util.ArrayList;

import com.fiuba.resume.heuristic.Heuristic;
import com.fiuba.resume.heuristic.birthDate.WithDateFormat;
import com.fiuba.resume.heuristic.birthDate.PreviousKeyword;
import com.fiuba.resume.BaseParser;

public class BirthDateParser extends BaseParser {

	public BirthDateParser(String resumeFile, String outputFile, String expectedResult,
			IntelligenceReference intelligenceReference) {
		super(resumeFile, outputFile, expectedResult, intelligenceReference);
	}
	
	protected String getName() {
		return "Birth date  Parser";
	}
	
	protected void analyzeParagraph(ResumeParagraph pr) {
		
	}

	protected ArrayList<Heuristic> createHeuristics() {
		return new ArrayList<Heuristic>(){{
        	add(new PreviousKeyword(intelligenceReference));
        	add(new WithDateFormat(intelligenceReference));
        }};
	}	
}
