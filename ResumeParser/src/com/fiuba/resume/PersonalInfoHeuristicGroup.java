package com.fiuba.resume;

import java.util.ArrayList;

import org.json.JSONObject;

public class PersonalInfoHeuristicGroup extends ParagraphTypeHeuristicGroup {

	public PersonalInfoHeuristicGroup(ArrayList<ResumeParagraph> paragraphs) {
		this.paragraphs = paragraphs;
		
		this.heuristicsForParagraphType = new ArrayList<ResumeParagraphTypeHeuristic>();
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicKeyword("PersonalInfo Keywords", paragraphs, "personalInfo.txt", true));
		this.heuristicsForParagraphType.add(new ResumeParagraphTypeHeuristicFullName("PersonalInfo Known names", paragraphs, "names.txt"));
		
		this.heuristicsForResumeAttribute = new ArrayList<ResumeAttributeHeuristic>();
		this.heuristicsForResumeAttribute.add(new PersonalInfoEmailHeuristic("PersonalInfo Email"));
		this.heuristicsForResumeAttribute.add(new PersonalInfoNameHeuristic("PersonalInfo Name", "names.txt"));
		this.heuristicsForResumeAttribute.add(new PersonalInfoBirthDateHeuristic("PersonalInfo BirthDate"));
	}
}
