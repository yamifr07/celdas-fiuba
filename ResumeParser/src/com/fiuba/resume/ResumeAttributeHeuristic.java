package com.fiuba.resume;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONObject;

public abstract class ResumeAttributeHeuristic {

	protected int tries = 0;
	protected int success = 0;
	protected String name;
	
	//each success value is a value between 1-100
	public void addSuccess(int success) {
		this.tries++;
		this.success += success;
	}
	
	public int getSuccessPorcentage() {
		return this.success / tries;
	}
	
	public ResumeAttributeHeuristic(String name) {
		this.name = name;
	}
	
	public void print() {
		System.out.println(this.name + ": " + (this.success / this.tries) + " % considering " + this.tries + " tries");
	}
	
	public abstract JSONObject extractInfo(ArrayList<ResumeParagraph> selectedParagraphs);
	
	protected int getWordCaseType(String word) {
		if (word.toLowerCase().equals(word)) {
			return 1;
		} else if (word.toUpperCase().equals(word)) {
			return 2;
		} else if (word.substring(0, 1).toUpperCase().concat(word.substring(1).toLowerCase()).equals(word)) {
			return 3;
		} else {
			return 4;
		}
	}
	
	protected boolean isUpperCase(String word) {
		return word.matches("[A-Z]+") && word.toUpperCase().equals(word);
	}
	
	protected ArrayList<String> getKeywords(String keywordsFile) {
		String workingDir = System.getProperty("user.dir");
		ArrayList<String> out = new ArrayList<String>();
    	BufferedReader br;
    	try {
    		br = new BufferedReader(new FileReader(workingDir + "/intelligence/paragraphType/" + keywordsFile));
    		String line;
    	    while ((line = br.readLine()) != null) {
    	    	if (line.trim().length() > 0) {
    	    		out.add(line.toLowerCase());
    	    	}
    	    }
    	    br.close();
    	} catch (IOException e) {
			System.out.println("The file of keywords was not found.");
			System.exit(0);
		}
		return out;
	}
}
