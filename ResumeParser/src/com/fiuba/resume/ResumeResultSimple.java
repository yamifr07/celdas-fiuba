package com.fiuba.resume;

public class ResumeResultSimple extends ResumeResult {

	private String name;
	
	public ResumeResultSimple(String name) {
		this.name = name;
	}
	
	public ResumeParagraphType getType() {
		return ResumeParagraphType.PERSONAL_INFO;
	}
	
	public String getString() {
		return this.name;
	}
}
