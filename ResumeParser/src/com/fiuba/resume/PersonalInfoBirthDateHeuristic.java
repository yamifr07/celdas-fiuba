package com.fiuba.resume;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONObject;

public class PersonalInfoBirthDateHeuristic extends ResumeAttributeHeuristic {
	
	public PersonalInfoBirthDateHeuristic(String name) {
		super(name);
	}
	
	private String getMonthsForRegex() {
		return "Ene|Enero|Feb|Febrero|Mar|Marzo|Abr|Abril|May|Mayo|Jun|Junio|Jul|Julio|Ago|Agosto|Sep|Septiembre|Set|Setiempre|Oct|Octubre|Nov|Noviembre|Dic|Diciembre";
	}
	
	public JSONObject extractInfo(ArrayList<ResumeParagraph> selectedParagraphs) {
		JSONObject out = new JSONObject();
		out.put("birthDate", "notFound");
		ArrayList<Pattern> patterns = new ArrayList<Pattern>();
		patterns.add(Pattern.compile("[0-9]{1,2}[\\\\-\\\\/][0-9]{1,2}[\\\\-\\\\/][0-9]{2,4}"));
		patterns.add(Pattern.compile("[0-9]{1,2}[de ]+[" + this.getMonthsForRegex() + "]+[de ]+[0-9]{2,4}"));
		for (ResumeParagraph resumeParagraph : selectedParagraphs) {
			for (Pattern p : patterns) {
				Matcher m = p.matcher(resumeParagraph.getText());
				if (m.find()) {
					out.put("birthDate", m.group());
					return out;
				}
			}
		}
		return out;
	}
}
