package com.fiuba.resume;

public enum ResumeParagraphType {

	EDUCATION, 
	PERSONAL_INFO, 
	WORK_HISTORY,
	HOBBIES,
	SKILLS;
}
