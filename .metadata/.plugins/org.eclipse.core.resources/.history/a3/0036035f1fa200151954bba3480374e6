package com.fiuba.resume;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ParseHelper {

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public String getResumeFile() {
		return resumeFile;
	}

	public void setResumeFile(String resumeFile) {
		this.resumeFile = resumeFile;
	}

	public ParseHelper(String resumeFile, String outputFile) {
		this.resumeFile = resumeFile;
		this.outputFile = outputFile;
	}

	public String getParagraphs() {
		return getParagraphs(-1);
	}

	public String getParagraphs(int limit) {
		InputStream is = null;
        try {
            is = new FileInputStream(new File(getResumeFile()));

            Parser parser = new AutoDetectParser();
            ContentHandler handler = new BodyContentHandler();

            Metadata metadata = new Metadata();

            parser.parse(is, handler, metadata, new ParseContext());

            if (limit == -1) {
            	return handler.toString();
            }
            String text = "";
            String[] paragraphs = handler.toString().split("\\n\\n");
            int length = paragraphs.length;
            if (limit >= length) {
            	return handler.toString();
            }
            for (int i = 0; i < limit && i < length; i++) {
            	text += paragraphs[i] + "\\n\\n";
            }
            return text;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
	}
	
	private String removeIgnoredWords(String input) {
		for (String ignored : this.getIgnoredRegex()) {
			input = input.replaceAll(ignored, "");
		}
		return input;
	}
	
	private ArrayList<String> getIgnoredRegex() {
		ArrayList<String> ignoredWords = new ArrayList<String>();
		ignoredWords.add("CURRICULUM");
		ignoredWords.add("VITAE");
		return ignoredWords;
	}

	public String reduceSpace(String s) {
		if (!s.contains("  ")) {
			return s;
		}
		return reduceSpace(s.replace("  ", " "));

	}

	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}

	private ArrayList<String> lines;
	private String resumeFile;
	private String outputFile;

}
