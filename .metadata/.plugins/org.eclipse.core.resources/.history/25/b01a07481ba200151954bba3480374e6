package com.fiuba.resume;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.poi.hwpf.HWPFDocument;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

public class ParseHelper {

	public String getOutputFile() {
		return outputFile;
	}

	public void setOutputFile(String outputFile) {
		this.outputFile = outputFile;
	}

	public String getResumeFile() {
		return resumeFile;
	}

	public void setResumeFile(String resumeFile) {
		this.resumeFile = resumeFile;
	}

	public ParseHelper(String resumeFile, String outputFile) {
		this.resumeFile = resumeFile;
		this.outputFile = outputFile;
	}

	public String getParagraphs() {
		return getParagraphs(-1);
	}

	public String getParagraphs(int limit) {
		
		InputStream is = null;

        try {
            is = new BufferedInputStream(new FileInputStream(new File(getResumeFile())));

            Parser parser = new AutoDetectParser();
            ContentHandler handler = new BodyContentHandler(System.out);

            Metadata metadata = new Metadata();

            parser.parse(is, handler, metadata, new ParseContext());

            for (String name : metadata.names()) {
                String value = metadata.get(name);

                if (value != null) {
                    System.out.println("Metadata Name:  " + name);
                    System.out.println("Metadata Value: " + value);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TikaException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch(IOException e) {
                    e.printStackTrace();
                }
            }
        }
    
		BufferedReader in;
		POIFSFileSystem fs = null;
		String inputText = new String();
		try {
			fs = new POIFSFileSystem(new FileInputStream(getResumeFile()));
			// Couldn't close the braces at the end as my site did not allow it
			// to close
			HWPFDocument doc = new HWPFDocument(fs);
			WordExtractor we = new WordExtractor(doc);
			String[] paragraphs = we.getParagraphText();
			for (int i = 0; i < paragraphs.length && (limit != -1 || i < limit); i++) {
				paragraphs[i] = paragraphs[i].replaceAll("\\cM?\r?\n?\t", "");
				// System.out.println(paragraphs[i]);
				String toInput = removeIgnoredWords(paragraphs[i]);
				if (toInput.trim().length() > 0) {
					inputText += ParserUtils.cleanUp(toInput) + ";";
				}
			}

		}
		// if the file is not found, the program displays an error message and
		// exits
		catch (IOException e) {
			System.out.println("The file " + getResumeFile() + " was not found.");
			System.exit(0);
		}
		return inputText;
	}
	
	private String removeIgnoredWords(String input) {
		for (String ignored : this.getIgnoredRegex()) {
			input = input.replaceAll(ignored, "");
		}
		return input;
	}
	
	private ArrayList<String> getIgnoredRegex() {
		ArrayList<String> ignoredWords = new ArrayList<String>();
		ignoredWords.add("CURRICULUM");
		ignoredWords.add("VITAE");
		return ignoredWords;
	}

	public String reduceSpace(String s) {
		if (!s.contains("  ")) {
			return s;
		}
		return reduceSpace(s.replace("  ", " "));

	}

	public void setLines(ArrayList<String> lines) {
		this.lines = lines;
	}

	private ArrayList<String> lines;
	private String resumeFile;
	private String outputFile;

}
